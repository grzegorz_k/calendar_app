from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import *
from .models import Event


def index(request):
    template_name = 'calendar_app/index.html'
    if request.user.is_authenticated:
        return redirect('/home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('/home')
            else:
                messages.info(request, "złe dane logowania")
    return render(request, template_name)


def logoutUser(request):
    logout(request)
    return redirect('/')


class DetailView(generic.DetailView):
    template_name = 'calendar_app/detail.html'
    model = Event


@login_required(login_url='/')
def home(request):
    template_name = 'calendar_app/home.html'
    event_list = Event.objects.all()
    context = {'event_list': event_list}
    return render(request, template_name, context)


@login_required(login_url='/')
def createEvent(request):
    form = EventForm()
    template_name = 'calendar_app/createEvent.html'
    if request.method == 'POST':
        form = EventForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/home')

    context = {'form': form}
    return render(request, template_name, context)


@login_required(login_url='/')
def updateEvent(request, pk):
    event = Event.objects.get(id=pk)
    form = EventForm(instance=event)
    template_name = 'calendar_app/updateEvent.html'

    if request.method == 'POST':
        form = EventForm(request.POST, instance=event)
        if form.is_valid():
            form.save()
            return redirect('/home')

    context = {'form': form}
    return render(request, template_name, context)


@login_required(login_url='/')
def deleteEvent(request, pk):
    event = Event.objects.get(id=pk)
    template_name = 'calendar_app/deleteEvent.html'
    context = {'event': event}

    if request.method == 'POST':
        event.delete()
        return redirect('/home')

    return render(request, template_name, context)
