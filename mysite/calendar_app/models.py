
from django.db import models


class Event(models.Model):
    title = models.CharField(max_length=200)
    start_date = models.DateTimeField("Czas początkowy")
    end_date = models.DateTimeField("Czas zakończenia")
    description = models.CharField(max_length=400, blank=True, default='')

    def __str__(self):
        return self.title


class RouteEvent(Event):
    start_location = models.CharField(max_length=40)
    end_location = models.CharField(max_length=40)

