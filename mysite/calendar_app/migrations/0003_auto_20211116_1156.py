# Generated by Django 3.2.8 on 2021-11-16 10:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('calendar_app', '0002_auto_20211116_1114'),
    ]

    operations = [
        migrations.RenameField(
            model_name='event',
            old_name='description',
            new_name='Opis',
        ),
        migrations.RenameField(
            model_name='event',
            old_name='title',
            new_name='Tytuł',
        ),
    ]
