from django.urls import path

from . import views

app_name = 'calendar_app'
urlpatterns = [
    path('', views.index, name='index'),#login
    path('logout/', views.logoutUser, name='logoutUser'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('createEvent/', views.createEvent, name='createEvent'),
    path('updateEvent/<int:pk>', views.updateEvent, name='updateEvent'),
    path('deleteEvent/<int:pk>', views.deleteEvent, name='deleteEvent'),
    path('home/', views.home, name='home'),
]